import React from "react";
import {Header} from './Components/Header/header'
import {TestAssignmentSection} from "./Components/TestAssignmentSection/TestAssignmentSection";
import {LetsGetAcquaintedSection} from "./Components/LetsGetAcquainted/LetsGetAcquaintedSection";
import {OurCheerfulUsersSection} from "./Components/OurCheerfulUsers/OurCheerfulUsersSection";
import {RegisterToGetAWorkSection} from "./Components/RegisterToGetAWork/RegisterToGetAWorkSection";

import  {FooterSection} from "./Components/Footer/FooterSection";

function App() {
  return (
    <div className="App">
      <Header/>
      <TestAssignmentSection/>
      <LetsGetAcquaintedSection/>
      <OurCheerfulUsersSection/>
      <RegisterToGetAWorkSection/>
      <FooterSection/>
    </div>
  );
}

export default App;
