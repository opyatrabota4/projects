import React from 'react';
import './Footer.css'

export const FooterSection = () => {
    return (
        <div className={'footerWrapper'}>
            <p className={'copyRight'}>&#169; abz.agency specially for the test task;</p>
            <div className={'footerBlackBlock'} />
            <div className={'abzAgencyLogo'} />
            <p className={'footerHomepageText'}>0201 - Homepage - 1024</p>
        </div>
    );
};

