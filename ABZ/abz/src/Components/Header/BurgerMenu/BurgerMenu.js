import React, {useEffect, useState} from 'react';
import './burgerMenu.css'

export const BurgerMenu = () => {
    const [burgerActive , setBurgerActive] = useState(false)
    const [open, setOpen] = useState(false)

    const links = document.getElementsByClassName('burgerMenuLink')
    const closeBurger = () => {
        setOpen(false)
        setBurgerActive(false)
    }

    // close burger and set overflow: unset if user changes width to more than 768px
    // WHILE burger is opened
    window.addEventListener('resize', function (event) {
        let width = window.innerWidth
        if (width > 769) {
            document.body.style.overflow = 'unset';
            setOpen(false)
            setBurgerActive(false)
        }
           });
    const showBurger = () => {
        setOpen(!open);
        setBurgerActive(!burgerActive)
    }
    // lock the scroll if burger is opened
    useEffect(() => {
        if (open) {
            document.body.style.overflow = 'hidden';
        } else {
            document.body.style.overflow = 'unset';
        }
    },[open])

    return (
        <div className={'mainBurgerWrapper'}>

            <div className='menuIcon' id="burgerIconThreeLines" onClick={() => showBurger()}>
                <span />
            </div>
            <nav onClick={closeBurger} className={`burgerMenuBody ${burgerActive ? 'active' : ''}`} id="burgerMenuNavBody">

                <ul className="burgerMenuList burgerLinksStyle">
                    {/*white block for logo to appear in burger and be higher than ul li lists*/}
                    <div className={'white'}/>
                    <li className={'paddingFromWhiteBlock'}><a href="#registration" className="burgerMenuLink">About me</a></li>
                    <li><a href="#registration" className="burgerMenuLink">Relationship</a></li>
                    <li><a href="#registration" className="burgerMenuLink">Users</a></li>
                    <li><a href="#registration" className="burgerMenuLink">Sign up</a></li>
                    <li><a href="#registration" className="burgerMenuLink">Terms and Conditions</a></li>
                </ul>
                <ul className="secondBurgerMenuList burgerLinksStyle">
                    <li><a href="#registration" className="burgerMenuLink">How it works</a></li>
                    <li><a href="#registration" className="burgerMenuLink">Partnership</a></li>
                    <li><a href="#registration" className="burgerMenuLink">Help</a></li>
                    <li><a href="#registration" className="burgerMenuLink">Level testimonial</a></li>
                    <li><a href="#registration" className="burgerMenuLink">Contact us</a></li>
                </ul>
                <ul className="thirdBurgerMenuList burgerLinksStyle">
                    <li><a href="#registration" className="burgerMenuLink">Articles</a></li>
                    <li><a href="#registration" className="burgerMenuLink">Our news</a></li>
                    <li><a href="#registration" className="burgerMenuLink">Testimonial</a></li>
                    <li><a href="#registration" className="burgerMenuLink">Licenses</a></li>
                    <li><a href="#registration" className="burgerMenuLink">Privacy Policy</a></li>
                </ul>
            </nav>
        </div>
    );
};

