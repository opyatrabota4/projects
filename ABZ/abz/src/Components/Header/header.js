
import React from 'react';
import './header.css'
import {BurgerMenu} from "./BurgerMenu/BurgerMenu";


export const Header = () => {

    return (
        <div className={'headerFixed'}>
            <div className={'headerWrapper'}>
                <div className={'logoWrapper'}>
                    <div className={'logoImitation'}/>
                    <h2 className={'headerLogo'}>testtask</h2>
                </div>
                <BurgerMenu/>
                <nav className={'navBarHeader'}>
                    <a href={'#registration'}>About me</a>
                    <a href={'#registration'}>Relationships</a>
                    <a href={'#registration'}>Requirements</a>
                    <a href={'#registration'}>Users</a>
                    <a href={'#registration'}>Sign Up</a>
                </nav>
            </div>
        </div>
    )
};