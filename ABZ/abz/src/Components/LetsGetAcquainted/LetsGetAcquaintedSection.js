import React from 'react';
import './LetsGetAcquainted.css'

export const LetsGetAcquaintedSection = () => {
    return (
        <div className={'LetsGetAcquaintedMainWrapper'}>
            <div className={'LetsGetAcquaintedSecondWrapper'}>
                <div className={'LetsGetAcquaintedImage'}/>
                <div className={'LetsGetAcquaintedTextWrapper'}>
                    <h1>Let's get acquainted</h1>
                    <h2>I'm a good front-end developer</h2>
                    <p>What defines a good front-end developer is one
                        that has skilled knowledge of HTML, CSS, JS with
                        a vast understanding of User design thinking as
                        they'll be building web interfaces with accessibility in mind.
                        They should also be excited to learn, as the world of Front-End
                        Development keeps evolving.</p>
                    <button><a href={'#registration'}>Sign up</a></button>
                </div>
            </div>
        </div>

    );
};

