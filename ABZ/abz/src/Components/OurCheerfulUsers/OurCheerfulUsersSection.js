import React, {useEffect, useState} from 'react';
import './OurCheerfulUsers.css'

export const OurCheerfulUsersSection = () => {
    const [users, setUsers] = useState([])
    const [pageOfUsers, setPageOfUsers] = useState(1)
    const [itemsToShow, setItemsToShow] = useState(6)
    const [expanded,setExpanded] = useState(false)
    const [showText, setShowText] = useState('show more')
    const [yes,setYes] = useState(false)

    let getUsers = fetch('https://frontend-test-assignment-api.abz.agency/api/v1/users?page='+pageOfUsers+'&count=45')
    useEffect(() => {
        let mounted = true;
            getUsers
            .then(response => {
                return response.json();
            })
            .then(data => {
                console.log(data);
                if (mounted) {
                    setUsers(data.users)
                    console.log(users)
                }
              })
        return () => mounted = false
    },[yes])

const showMore = () => {
    if (itemsToShow < 9) {
        setItemsToShow(9)
        setExpanded(true)
        // remove button if there is no more users to show
        // const button = document.getElementById('OurCheerfulUsersButtonShowMore')
        // button.disabled = true
        // button.style.visibility = 'hidden'
        setShowText('Show less')
    }
    else {
        setItemsToShow(6)
        setExpanded(false)
        setShowText('Show more')
      }
    };

if (users) {
    return  <div className={'OurCheerfulUsersWrapper'}>
        <div className={'OurCheerfulUsersSecondWrapper'}>
        <h1>Our cheerful users</h1>
        <h2>The best specialists are shown below</h2>
        <ul id={'ul'} className={'OurCheerfulUsersUl'}>
            {users.slice(0, itemsToShow).map((item ,index) => (
                <li className={'OurCheerfulUsersLi'} key={index}>
                    <img alt={'#'} src={item.photo} className={'OurCheerfulUsersPhoto'} />
                            <p className={'userName'}>{item.name}
                                <span className="tooltiptext">{item.name}</span>
                            </p>
                            <p className={'userPosition'}>{item.position}</p>
                            <p className={'userEmail'}>{item.email}
                            <span className="tooltiptext">{item.email}</span>
                            </p>
                            <p className={'userPhone'}>{item.phone}</p>
                </li>
            ))}
        </ul>
        <button onClick={() => showMore()} id={'OurCheerfulUsersButtonShowMore'} className={'OurCheerfulUsersButtonShowMore'}>
            {expanded ? <span>{showText}</span> : <span>{showText}</span>}
        </button>
        </div>
    </div>
}
else {
    return (
        <div className={'OurCheerfulUsersWrapper'}>
            <h1>Our cheerful users</h1>
            <h2>The best specialists are shown below</h2>
            <p>an error occurred...</p>
        </div>
    );
}

};

