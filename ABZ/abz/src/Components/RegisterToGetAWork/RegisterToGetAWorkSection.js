import React, {useEffect, useState} from 'react';
import './RegisterToGetAWork.css'
import img from './CatTrack-removebg-preview.png'

export const RegisterToGetAWorkSection = () => {
    const [positions, setPositions] = useState([])
    const [yes, setYes] = useState(false)
    const [token, setToken] = useState()
    const [uploadText, setUploadText] = useState('Upload your photo')


    // const test = (e) => {
    //     e.preventDefault()
    //     console.log('form sent')
    //
    //     console.log(token)
    //     fetch('https://frontend-test-assignment-api.abz.agency/api/v1/token')
    //         .then(function(response) { return response.json(); })
    //         .then(function(data) {
    //             console.log(data);
    //             setToken(data.token)
    //         })
    //         .catch(function(error) {
    //             console.error(error)
    //         })
    //
    //     const formData = new FormData();
    //     const fileField = document.querySelector('input[type="file"]');
    //     formData.append('position_id', 2);
    //     formData.append('name', 'Jhon');
    //     formData.append('email', 'Jhon@gmail.com');
    //     formData.append('phone', '+380955388485');
    //     formData.append('photo', fileField.files[0]);
    //     fetch('https://frontend-test-assignment-api.abz.agency/api/v1/users',{
    //         method: 'POST',
    //         body: formData,
    //         headers:
    //             {
    //                 'Token': token,
    //             },})
    //         .then(response => {
    //             return response.json();
    //         })
    //         .then(data => {
    //             console.log(data);
    //               if(data.success) {
    //                   console.log(data + ' success')
    //               } else {
    //
    //           }
    //         })
    //        }

    useEffect(() => {
        // fetch('https://frontend-test-assignment-api.abz.agency/api/v1/token')
        //         //     .then(function(response) {
        //         //         return response.json()
        //         //     })
        //         //     .then(function(data) {
        //         //         console.log(data);
        //         //         setToken(data.token)
        //         //         console.log(token)
        //         //     })
        let mounted = true
        fetch('https://frontend-test-assignment-api.abz.agency/api/v1/positions')
            .then(response => {
                return response.json();
            })
            .then(data => {
                console.log(data)
                if (mounted) {
                    setPositions(data.positions)
                    console.log(positions);
                }
            })
        return () => mounted = false
    }, [yes])

    const imageUpload = () => {
        const uploadedFile = document.getElementById('fileUpload').files[0]
        setUploadText(uploadedFile.name)
    }
    if (positions) {
        return (
            <div className={'RegisterToGetAWorkWrapper'}>
                {/*<form onSubmit={test}>*/}
                <form>
                    <p><a name={'registration'}/></p>
                    <h1>Register to get a work</h1>
                    <h2 className={'paragraphYourPersonalData'}>Your personal data is stored according to the Privacy
                        Policy</h2>
                    <input required={true} min={2} max={60} type={'text'} className={'inputForInfo'} placeholder={'Your name'}/>
                    <input required={true} type={'email'} className={'inputForInfo'} placeholder={"Email"}
                           pattern={'^(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\\])$)'}
                    />
                    <input required={true} type={'tel'} pattern="^[\+]{0,1}380([0-9]{9})$)" className={'inputForInfo inputForInfoLast'}
                           placeholder={'Phone Format: +380955388485'}/>
                    <div className={'yourPositionAndUploadWrapper'}>
                        <p className={'selectYourOption'}>Select your option</p>
                        {positions.map(item => (
                            <div key={item.id} className={'radioButtonWrapper'}>
                                <label htmlFor={item.id} className='customRadioButton'>
                                    <input required={true} className={'radioButtonInput'} id={item.id}
                                           name={'position'} type={'radio'} value={item.name} />
                                    <span className={'checkMark'} />
                                </label>
                                <span className={'positionName'}>{item.name}</span>
                            </div>
                        ))}
                       <div className={'uploadWrapper'} >
                            <span className="btn-file" >
                             Upload<input type="file" id={'fileUpload'} onChange={() => imageUpload()} accept="image/png, image/jpeg"/>
                            </span>
                           <label htmlFor={'file'}>{uploadText}
                           <span className="tooltiptextUpload">{uploadText}</span>
                           </label>

                       </div>
                    </div>
                    <button className={'submitButton'} type={'submit'}>Sign up</button>
                    <img className={'catTrack'} src={img}  alt={'#'}/>
                </form>

            </div>
        );
    }
    else {
        return (
            <div>
                <p>an error occurred...</p>
            </div>
        )
    }
};
