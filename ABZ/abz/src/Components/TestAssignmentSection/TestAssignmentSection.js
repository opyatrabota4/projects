import React, {useEffect, useState} from 'react';
import './testAssignment.css'

export const TestAssignmentSection = () => {
    const [text ,setText] = useState('Front-end developers make sure the user ' +
        'sees and interacts with all the necessary elements to ensure conversion. ' +
        'Therefore, responsive design, programming languages and specific frameworks are ' +
        'the must-have skillsets to look for when assessing your front-end developers.')
    const [text2 ,setText2] = useState('Front-end developers make sure the user sees and interacts ' +
        'with all the necessary elements to ensure conversion')

    useEffect(() => {
        window.addEventListener('resize', function (event) {
            const testAssignmentText = document.getElementById('testAssignmentText')
            let width = window.innerWidth
            if (width < 769) {
                testAssignmentText.innerText = text2
            }
            else {
                testAssignmentText.innerText = text
            }
        });
    },[])


    return (
        <div className={'testAssignmentSection'}>
            <div className={'testAssignmentContainer'}>
                <div className={'testAssignmentWrapper'}>
                    <h1 className={'testAssignmentHeading'}>Test assignment for front-end developers</h1>
                    <p className={'testAssignmentText'} id={'testAssignmentText'}>{window.innerWidth < 768 ? text2 : text}
                    </p>
                    <button className={'testAssignmentButtonSignUp'}><a href={'#registration'}>Sign up</a></button>
                </div>
            </div>
        </div>
    );
};

