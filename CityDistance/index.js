

function chooseDistance(array,t,k) {
    const subsets = [[]];
    let sum = [];

    for (const el of array) {
        const last = subsets.length-1;
        for (let i = 0; i <= last; i++) {
            subsets.push( [...subsets[i], el] );
        }
    }
    for (let i = 0; i < subsets.length; i++) {

        if (subsets[i].length === k){
            let reducer = subsets[i].reduce((a,b) => a + b,0);
            if (reducer < t) {
                sum.push(reducer)
            }
        }
        else{
            console.log('Не найдены варианты при выбранном максимальном расстоянии путешествия и минимальномм количестве городов которые нужно посетить. ' + subsets[i])
        }
    }
    let max = Math.max.apply(Math,sum);

    if (sum.length >=1) {
        console.log(sum);
        console.log('Лучший вариант ' + max);
        console.log('Номер в массиве ',sum.indexOf(max));
    }
    else{
        console.log('Увеличьте показатель "t" или уменьшите показатель "k"')
    }


    return subsets.slice(1);
}
console.log(chooseDistance([51, 56, 58, 59, 61],174,3));


