
const input = document.getElementById('inputTodo');
const btn = document.getElementById('btn');
const result = document.getElementById('ulTodo');
const arch = document.getElementById('ulArch');


btn.addEventListener('click', (e) => {
    if(input.value === '') return;
    createDelete(input.value);
    input.value = ''
});

    //create and delete to do
function createDelete(value) {

    const li = document.createElement('li');
    result.appendChild(li);
    // li.textContent = value;
    const para = document.createElement('p')
    para.textContent = value;
    para.className = 'para';
    li.appendChild(para);

    // create text holder for category option
    const category = document.getElementById('category').value;

    li.className = category; // set li with specific class name

    let text = document.createTextNode(category);
    const span = document.createElement('span');
    span.className = 'category';
    span.appendChild(text);
    li.appendChild(span);

    //get time
    let localTime = document.createElement('p');
    let today = new Date();
    let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    localTime.innerHTML = `Создано в  ${new Date().toLocaleTimeString()} ${date}`;
    li.append(localTime);
    // edit button
    const editButton  = document.createElement('button');
    editButton.textContent = 'редактировать';
    li.appendChild(editButton);

    editButton.addEventListener('click',e => {
        editButton.disabled = true
        const input  = document.createElement('input')
        li.appendChild(input)
        input.placeholder = 'изменить текст заметки'
        const select = document.createElement('select')
        li.appendChild(select)
        const option = document.createElement('option')
        option.textContent = 'Task'
        select.appendChild(option)
        const option2 = document.createElement('option')
        option2.textContent = 'Random Thought'
        select.appendChild(option2)
        const option3 = document.createElement('option')
        option3.textContent = 'Idea'
        select.appendChild(option3)
        const save = document.createElement('button')
        save.textContent = 'save changes'
        li.appendChild(save)
        save.onclick = function () {
            if (input.value === '') {
                alert('заполните пустое поле')
            }
            else{
                para.innerHTML = input.value
                span.textContent = select.value

                input.style.display = 'none'
                select.style.display = 'none'
                save.style.display = 'none'
                editButton.disabled = false
            }
        }
    });

    //Archive button
    const addToArchButton  = document.createElement('button');
    addToArchButton.textContent = "в архив";
    li.appendChild(addToArchButton);

    addToArchButton.addEventListener('click',(e) => {
        result.contains(li) ? arch.appendChild(li) : result.appendChild(li);
        addToArchButton.textContent === 'в архив' ? addToArchButton.textContent = 'убрать из архива' : addToArchButton.textContent = 'в архив'
    });

    //remove button
    const btn = document.createElement('button');
    btn.className = 'remove-button';
    btn.textContent = 'remove';
    li.appendChild(btn);


    //remove to do element AND change class for active li element

    btn.addEventListener('click', (e) => {
       result.contains(li) ? result.removeChild(li) : arch.removeChild(li);
    }) ;
    li.addEventListener('click',(e) => {
        li.classList.toggle('li-active')
    });
    result.append(li)
}

const sortButton = document.getElementById('sort-button');
const sortOption = document.getElementById('sort-option');

sortButton.addEventListener('click', ev => {
    sort(sortOption.value )
});
function sort(value) {
    // console.log(value)
    const category = document.getElementsByClassName('category');
    const array = [];

    let li = document.querySelectorAll('li');
    li.forEach(function(item) {
        array.push(item)
    });

    array.forEach(function (e) {
        if (e.className === value) {
            e.style.display = 'block'
        }
        else if (value === 'All'){
            e.style.display = 'block'
        }
        else{
            e.style.display = 'none'
        }
    })


}


function getTime() {
    let today = new Date();
    let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

}