
const setPasswordLengthInput = document.getElementById('setPasswordLengthInput');
const setUseNumbers = document.getElementById('setNumbersOnly');
const setUseLowerCase = document.getElementById('setUseLowerCase')
const setUseUpperCase = document.getElementById('setUseUpperCase')

let symbolsToGenerateWith = '';

const numbersTo = '0123456789';
const lowercaseLetters = 'qwertyuiopasdfghjklzxcvbnm';
const uppercaseLetters  = 'QWERTYUIOPASDFGHJKLZXCVBNM';

let passwordLength = 5;
let password = '';


function generatePassword() {
    password = '';
    symbolsToGenerateWith = '';

    // symbolsToGenerateWith = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';

    if (setUseNumbers.checked) {
        symbolsToGenerateWith = symbolsToGenerateWith.concat(numbersTo)
        console.log(symbolsToGenerateWith)
    }
     if (setUseLowerCase.checked) {
         symbolsToGenerateWith = symbolsToGenerateWith.concat(lowercaseLetters)
         console.log(symbolsToGenerateWith)
     }
    if (setUseUpperCase.checked) {
        symbolsToGenerateWith = symbolsToGenerateWith.concat(uppercaseLetters)
        console.log(symbolsToGenerateWith)
    }
    if (!setUseNumbers.checked && !setUseLowerCase.checked && !setUseUpperCase.checked) {
        alert("выберите хотя бы одно значение")
        return
    }

        if (setPasswordLengthInput && setPasswordLengthInput.value < 15 && setPasswordLengthInput.value > 0) {
            passwordLength = setPasswordLengthInput.value
            alert('success')
        }
        else {
            setPasswordLengthInput.value = 15;
            passwordLength = setPasswordLengthInput.value
            alert('mistake')
        }
    passwordLength = setPasswordLengthInput.value;
    for (let  i = 0; i < passwordLength; i++) {
        const randomNumber = Math.floor(Math.random() * symbolsToGenerateWith.length);

        password +=  symbolsToGenerateWith.substring(randomNumber, randomNumber + 1)

    }
    document.getElementById('placeholder-for-password').value = password;
}


//////////////////////////////////////////////////////// TIMER



const result = document.getElementById('result');


const countDownDate = new Date('October 26, 2023 18:42:00').getTime();

let x = setInterval(function () {
    let now = new Date().getTime();

    let difference = countDownDate - now;


    let days = Math.floor(difference / (1000 * 60 * 60 * 24));
    let hours = Math.floor((difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((difference % (1000 * 60)) / 1000);

    result.innerHTML = days + " дней " + hours + " часов " + minutes + " минут " + seconds + " секунд ";


    if (difference < 0) {
        clearInterval(x);
        result.innerHTML = 'время вышло'
    }
},1000);




/////////////////////////////////////////// FETCH


// fetch("http://api.weatherstack.com/current?access_key=e246d8ab763ec266bbc74fb361d49e88&query=New York")
//     .then(res => res.json()
//         .then(data => {
//             console.log(data)
//         }));





//// create search input and button
// let divForSearch = document.createElement('div');
// let searchInput = document.createElement('input');
// let searchButton = document.createElement('button');
// searchButton.innerHTML = 'search';

// divForSearch.append(searchInput);
// divForSearch.append(searchButton);
// root.append(divForSearch);
// divForSearch.className = 'divForSearch';
let searchInput = document.getElementById('searchFilm');
const searchButton = document.getElementById('search')

// searchButton.addEventListener('click', search);
window.onload=function(){
    searchButton.click();
};

function search() {

    const root = document.getElementById('root');
    if (root) {
        root.remove()
    }


    fetch("https://imdb8.p.rapidapi.com/auto-complete?q=" + searchInput.value , {
        "method": "GET",
        "headers": {
            "x-rapidapi-host": "imdb8.p.rapidapi.com",
            "x-rapidapi-key": "6518aa6e2emsh4ac1e16a1c76e9fp1d4e63jsneeb6c8500a6b"
        }
    })
        .then(res => res.json()
            .then(data => {
                console.log(data.d);

                const rootDiv = document.createElement('div');
                rootDiv.setAttribute('id', 'root');
                document.body.append(rootDiv)

                const divWrapper = document.createElement('div');
                divWrapper.className = 'main-root-wrapper';

                rootDiv.append(divWrapper);

                for (let i = 0; i < data.d.length; i++) {
                    // console.log(data.d[i].l)
                    let divForEachP = document.createElement('div');
                    //// create new div for every iteration
                    divWrapper.append(divForEachP);

                    divForEachP.className = data.d[i].id;
                    let pForName = document.createElement('p');
                    let pForCast = document.createElement('p');
                    let imageHolder = document.createElement('img');

                    let randomColor = Math.floor(Math.random() * 16777215).toString(16);
                    divForEachP.style.backgroundColor = 'grey';

                    let classNameForName = data.d[i].l;

                    pForName.className = classNameForName.split(' ').join('');
                    pForName.innerHTML = data.d[i].l;

                    pForCast.className = classNameForName.split(' ').join('') + 'Cast';
                    pForCast.innerHTML = data.d[i].s;

                    imageHolder.className = classNameForName.split(' ').join('') + 'Image';
                    imageHolder.src = data.d[i].i.imageUrl;
                    imageHolder.style.width = '250px';

                    divForEachP.append(pForName);
                    divForEachP.append(pForCast);
                    divForEachP.append(imageHolder);
                }

            }));

}
// let my = [1,2,3,4,5,6,7]
// let pricess = [7,1,5,3,6,4]
// let prices2 = [7,5,3,2,1]
//
// let maxProfit = function (prices) {
//
//     let resultr = 0
//     for (let i = 0; i < prices.length; i++) {
//         if (prices[i] > prices[i-1]) {
//             resultr += prices[i] - prices[i-1]
//         }
//     }
//     return resultr
// }
//
// console.log(maxProfit(my))
// console.log(maxProfit(pricess))
// console.log(maxProfit(prices2))
