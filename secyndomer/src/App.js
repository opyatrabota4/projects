import React from 'react'
import WatchContainer from "./Components/WatchContainer/WatchContainer";
import './App.css'


function App() {
  return (
      <div className='app'>
        <WatchContainer/>
      </div>
  );
}

export default App;
