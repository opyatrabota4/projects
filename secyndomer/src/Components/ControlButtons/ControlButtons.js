import React from "react";
import "./ControlButtons.css";

export default function ControlButtons(props) {
    const StartButton = (
        <div className="btn btn-one btn-start"
             onClick={props.handleStart}>
            Старт
        </div>
    );
    const ActiveButtons = (
        <div className="buttons-container">
            <div className="btn btn-two"
                 onClick={props.handleReset}>
                Сброс
            </div>
            <div className="btn btn-one"
                 onClick={props.handlePauseResume}>
                {props.isPaused ? "Возобновить" : "Пауза"}
            </div>
        </div>
    );

    return (
        <div className="control-Buttons">
            <div>{props.active ? ActiveButtons : StartButton}</div>
        </div>
    );
}