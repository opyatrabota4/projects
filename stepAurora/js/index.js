
let menuIcon = document.getElementById('burgerIconThreeLines')
let burgerMenuBody = document.getElementById('burgerMenuNavBody')

menuIcon.addEventListener('click', () => {
    document.body.classList.toggle('lockScroll')
    menuIcon.classList.toggle('active')
    burgerMenuBody.classList.toggle('active')
})

const closeIconBurger = document.getElementById('closeIconBurger')
closeIconBurger.addEventListener('click', () => {
    console.log(1)
    document.body.classList.remove('lockScroll')
    menuIcon.classList.remove('active')
    burgerMenuBody.classList.remove('active')
})

// Footer arrows

const arrowFooterAboutsUs = document.getElementById('arrowFooterToggleAboutUs')
const containerForLinksAboutUs = document.getElementById('containerForLinksAboutUsID')
arrowFooterAboutsUs.addEventListener('click', () => {
    containerForLinksAboutUs.classList.toggle('active')
    arrowFooterAboutsUs.classList.toggle('active')
})

const arrowFooterCategories = document.getElementById('arrowFooterToggleCategories')
const containerForLinksCategories = document.getElementById('containerForLinksCategoriesID')
arrowFooterCategories.addEventListener('click', () => {
    containerForLinksCategories.classList.toggle('active')
    arrowFooterCategories.classList.toggle('active')
})

const arrowFooterHelp = document.getElementById('arrowFooterToggleHelp')
const containerForLinksHelp = document.getElementById('containerForLinksHelpID')
arrowFooterHelp.addEventListener('click', () => {
    containerForLinksHelp.classList.toggle('active')
    arrowFooterHelp.classList.toggle('active')
})

const arrowFooterPayments = document.getElementById('arrowFooterTogglePayments')
const containerForLinksPayments = document.getElementById('containerForLinksPaymentsID')
arrowFooterPayments.addEventListener('click', () => {
    containerForLinksPayments.classList.toggle('active')
    arrowFooterPayments.classList.toggle('active')
})


// Footer arrows finished

// imitation of logging into page
const signInAndRegistration = document.getElementById('signInAndRegistrationID')
const signInText = document.getElementById('signInTextID')
    const registrationTopText =document.getElementById('registrationTopTextID')

const inputMainSectionEmail = document.getElementById('inputMainSectionEmailID')
const buttonRegisterMain = document.getElementById('buttonRegisterMain')

buttonRegisterMain.addEventListener('click', () => {
    if (inputMainSectionEmail.value === 'admin@gmail.com' ) {
        signInText.remove()
        registrationTopText.remove()
        const p = document.createElement('p')
        p.innerHTML = 'You are logged in as admin'
        p.classList.add('loggedText')
        signInAndRegistration.append(p)
        inputMainSectionEmail.value = ''
    }
})
// imitation of logging into page

