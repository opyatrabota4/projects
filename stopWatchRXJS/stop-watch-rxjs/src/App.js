import React from 'react'
import {StopWatchContainer} from './components/StopWatchContainer'


function App() {
  return (
    <div>
      <StopWatchContainer/>
    </div>
  );
}

export default App;
