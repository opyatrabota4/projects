import React, {useEffect, useState} from 'react';
import {from, fromEvent, Observable, of, interval, Subject, take, filter} from 'rxjs';
import {scan, takeUntil, catchError, map, tap} from "rxjs/operators";
import {Timer} from "./Timer";
import {TimerButtons} from "./TimerButtons";
import './stopWatchContainer.css'


export  const StopWatchContainer = () => {

    const [active, setActive] = useState(false);
    const [paused, setIsPaused] = useState(true);
    const [time, setTime] = useState(0);

    useEffect(() => {
        const observable = new Subject();
        interval(10).pipe(takeUntil(observable)).subscribe(() => {
                if (active && paused === false) {
                    setTime(val => val + 1);
                }
            });
        return () => {
            observable.next();
            observable.complete();
        };
    }, [paused,active]);


    const handleStart = () => {
        setActive(true);
        setIsPaused(false);
    }


    const handleResume = () => {
        setIsPaused(!paused)
    }


    const handleReset = () => {
        setActive(false);
        setTime(0);
    }
    return (
        <div className="watch-container">
            <p>Секундомер</p>
            <Timer time={time} />
            <TimerButtons
                active={active}
                paused={paused}
                handleStart={handleStart}
                handlePauseResume={handleResume}
                handleReset={handleReset}
            />
        </div>
    );
}

