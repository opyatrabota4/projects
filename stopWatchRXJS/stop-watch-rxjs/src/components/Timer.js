import React from 'react';
import './Timer.css'


export  const Timer = (props) => {
    return (
        <div>
             <span>{('0' + Math.floor(props.time / 6000)).slice(-2)}</span>&nbsp;:&nbsp;
             <span>{('0' + Math.floor((props.time / 100) % 60)).slice(-2)}</span>&nbsp;:&nbsp;
             <span>{('0' + Math.floor(props.time % 100)).slice(-2)}</span>
         </div>
    );
};

