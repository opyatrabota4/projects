import React from 'react';
import './TimerButtons.css'


export const TimerButtons = (props) => {
    const StartButton = (
        <div className="btn"
             onClick={props.handleStart}>
            Старт
        </div>
    )
        const ActiveButtons = (
            <div className="buttons-container">
                <div className="btn"
                     onClick={props.handleReset}>
                    Сброс
                </div>
                <div className="btn"
                     onClick={props.handlePauseResume}>
                    {props.paused ? "Возобновить" : "Пауза"}
                </div>
            </div>
        )

    return (
        <div>
            <div>{props.active ? ActiveButtons : StartButton}</div>
        </div>
    );
};

